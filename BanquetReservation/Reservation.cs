﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanquetBooking
{
    public class Reservation
    {
        public string reservationDate;
        public string reservationSession;
        public int reservationGuestNum;
        public List<string> reservationConfirmationTo;
        public Client reservationClient;
        public Banquet reservationLocation;
        public List<string> reservationFeatures;
        public bool reservationPromotion;

        public Reservation(string rDate, string rSession, int rGuestNum, List<string> rConfirmation, Client rClient, Banquet rLocation, List<string> rFeatures, bool rPromotions)
        {
            reservationDate = rDate;
            reservationSession = rSession;
            reservationGuestNum = rGuestNum;
            reservationConfirmationTo = rConfirmation;
            reservationClient = rClient;
            reservationLocation = rLocation;
            reservationFeatures = rFeatures;
            reservationPromotion = rPromotions;
        }

        public string PrintReservation()
        {
            string reservationInfo = "<h2>Reservation Info</h2>";
            reservationInfo += "Thank you  " + reservationClient.ClientName + " for making a reservation!" + "<br/>";
            reservationInfo += "Your email for your reservation is  " + reservationClient.ClientEmail + ". <br/>";
            reservationInfo += "Your phone number for your reservation is " + reservationClient.ClientPhone + ". <br/><br/>";

            reservationInfo += "You have made your reservation for " + reservationGuestNum + " guests at the " + reservationLocation.BanquetLocation + " on " + reservationDate + ". <br/>";
            reservationInfo += "It will be the " + reservationSession + "<br/><br/>";
            reservationInfo += PrintConfirmationMethod();
            reservationInfo += PrintPromotion();
            reservationInfo += "<br/> Your total price for the reservation is $" + CalcReservationPrice() + ". <br/> <br/>";

            string banquetInfo = "<h2>Banquet Contact Info</h2>";
            banquetInfo += "<p>Feel free to contact us if you have any questions.</p>";
            banquetInfo += "Banquet Name: " + reservationLocation.BanquetLocation + "<br/>";
            banquetInfo += "Banquet Email: " + reservationLocation.BanquetEmail + "<br/>";
            banquetInfo += "Banquet Phone: " + reservationLocation.BanquetPhoneNumber + "<br/>";
            banquetInfo += "Banquet Address: " + reservationLocation.BanquetAddress + "<br/><br/>";


            return reservationInfo + banquetInfo;
        }

        public int CalcReservationPrice()
        {
            int price = 0;

            //base price for session
            if (reservationSession == "Morning Session which takes place between 9am-3pm.")
            {
                price += 1200;
            }
            else if (reservationSession == "Noon Session which takes place between 12pm-6pm.")
            {
                price += 1500;
            }
            else if (reservationSession == "Evening Session which takes place between 6pm-12am.")
            {
                price += 2000;
            }

            //additional features/costs
            foreach (string s in reservationFeatures)
            {
                if (s == "Open bar (+$500)")
                {
                    price += 500;
                }
                if (s == "Table centerpieces (+$100)")
                {
                    price += 100;
                }
                if (s == "Floral room decorations(+$150)")
                {
                    price += 150;
                }
            }

            return price;
        }

        public string PrintPromotion()
        {
            if (reservationPromotion)
            {
                return "You have signed up to receieve information about any promotions for our banquet hall locations. They will be sent to your email/phone! <br/>";
            }
            else
            {
                return " ";
            }
        }

        public string PrintConfirmationMethod()
        {
            if (reservationConfirmationTo.Count == 0)
            {
                return " ";
            }
            else
            {
                return "A confirmation message will be sent shortly to: " + String.Join(", ", reservationConfirmationTo.ToArray()) + ". <br/>"; //from your code
            }
        }
    }
}
