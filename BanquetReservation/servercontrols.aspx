﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrol.aspx.cs" Inherits="BanquetBooking.servercontrol" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
 <form id="form1" runat="server">
        <div>
          <h1>Beverly's Banquet Hall Reservation Form</h1>
          <p>Welcome! Beverly's Banquet Hall is one of the most beautiful banquets in Canada. Our locations are very spacious and can hold up to 300 people. Catering is provided. </p>
          <asp:Label ID="clientNameLabel" AssociatedControlID="clientName" runat="server" Text="Name:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="clientName" runat="server" placeholder="Name"></asp:TextBox>
          <asp:RequiredFieldValidator ID="clientNameValidator" runat="server"  ControlToValidate="clientName" ErrorMessage="Please enter a name"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="clientNameRegex" runat="server" ValidationExpression="^[a-zA-Z- ]+$" ControlToValidate="clientName" ErrorMessage="Name must contain only letters"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="clientEmailLabel" AssociatedControlID="clientEmail" runat="server" Text="Email:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="clientEmail" runat="server" placeholder="Email"></asp:TextBox>
          <asp:RequiredFieldValidator ID="clientEmailValidator" runat="server"  ControlToValidate="clientEmail" ErrorMessage="Please enter an email address"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="emailRegex" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid email format"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="clientPhoneLabel" AssociatedControlID="clientPhone" runat="server" Text="Phone Number:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="clientPhone" runat="server" placeholder="Phone Number"></asp:TextBox>
          <asp:RequiredFieldValidator ID="clientPhoneValidator" runat="server"  ControlToValidate="clientPhone" ErrorMessage="Please enter a phone number"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="phoneRegex" runat="server" ValidationExpression="\d{10}" ControlToValidate="clientPhone" ErrorMessage="Invalid phone format, must be 10 digits"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="reservationDateLabel" AssociatedControlID="reservationDate" runat="server" Text="Reservation Date:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="reservationDate" runat="server" TextMode="Date"></asp:TextBox>
          <asp:RequiredFieldValidator ID="reservationTimeValidator" runat="server"  ControlToValidate="reservationDate" ErrorMessage="Please select a reservation date"></asp:RequiredFieldValidator>
          <asp:CustomValidator ID="dateRangeValidator" runat="server" onservervalidate="DateValidator" ControlToValidate="reservationDate" ErrorMessage="Invalid date. The date of the reservation must be a future date."></asp:CustomValidator>
          <br />

          <asp:Label ID="sessionType" AssociatedControlID="sessionList" runat="server" Text="Session:" Font-Bold="true"></asp:Label>
          <asp:DropDownList ID="sessionList" runat="server" DataTextField="Session">
            <asp:ListItem Value="Morning Session which takes place between 9am-3pm." Text="Morning Session (9am-3pm) $1200"></asp:ListItem>
            <asp:ListItem Value="Noon Session which takes place between 12pm-6pm." Text="Noon Session (12pm-6pm) $1500"></asp:ListItem>
            <asp:ListItem Value="Evening Session which takes place between 6pm-12am." Text="Evening Session(6pm-12am) $2000"></asp:ListItem>
          </asp:DropDownList>
          <asp:RequiredFieldValidator ID="sessionValidator" runat="server" ControlToValidate="sessionList" ErrorMessage="Please select a session"></asp:RequiredFieldValidator>
          <br />

          <asp:Label ID="reservationGuestNumberLabel" AssociatedControlID="reservationGuestNumber" runat="server" Text="Number of Guests:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="reservationGuestNumber" runat="server" placeholder="Number of Guests"></asp:TextBox>
          <asp:RequiredFieldValidator ID="reservationGuestValidator" runat="server" ControlToValidate="reservationGuestNumber" ErrorMessage="Please enter a guest number"></asp:RequiredFieldValidator>
          <asp:RangeValidator ID="reservationGuestRange" runat="server" ControlToValidate="reservationGuestNumber" MinimumValue="50" MaximumValue="300" Type="Integer" ErrorMessage="Guest number must be between 50 and 300"></asp:RangeValidator>
          <br />

          <asp:Label ID="banuqetLocationLabel" AssociatedControlID="banquetLocationList" runat="server" Text="Location:" Font-Bold="true"></asp:Label>
          <asp:DropDownList ID="banquetLocationList" runat="server" DataTextField="Location">
            <asp:ListItem Value="Richmond Hill" Text="Richmond Hill"></asp:ListItem>
            <asp:ListItem Value="Markham" Text="Markham"></asp:ListItem>
            <asp:ListItem Value="Toronto" Text="Toronto"></asp:ListItem>
          </asp:DropDownList>
          <asp:RequiredFieldValidator ID="banquetLocationValidator" runat="server" ControlToValidate="banquetLocationList" ErrorMessage="Please select a Location"></asp:RequiredFieldValidator>
          <br />

          <asp:Label ID="reservationConfirmationLabel"  AssociatedControlID="confirmationOptionsList" runat="server" Text="Send Reservation Confirmation Message To:" Font-Bold="true"></asp:Label>
          <div id="confirmationOptionsList" runat="server">
           <asp:CheckBox ID="confirmationText" runat="server" Text="Phone via Text" />
           <asp:CheckBox ID="confirmationCall" runat="server" Text="Phone via Call" />
           <asp:CheckBox ID="confirmationEmail" runat="server" Text="Email" />
          </div>
          <br />

         <asp:Label ID="featuresLabel"  AssociatedControlID="featuresList" runat="server" Text="Additional Features:" Font-Bold="true"></asp:Label>
          <div id="featuresList" runat="server">
           <asp:CheckBox ID="openBar" runat="server" Text="Open bar (+$500)" />
           <asp:CheckBox ID="centerpieces" runat="server" Text="Table centerpieces (+$100)" />
           <asp:CheckBox ID="floralDecorations" runat="server" Text="Floral room decorations(+$150)" />
          </div>
          <br />

          <asp:Label id="banquetPromotionLabel" AssociatedControlID="banquetPromotion" runat="server" Text="Would you like to signup to receive emails/texts about future promotions for our banquet hall?" Font-Bold="true"></asp:Label>
          <asp:RadioButtonList ID="banquetPromotion" runat="server" >
            <asp:ListItem Text="Yes" Value="Yes" />
            <asp:ListItem Text="No" Value="No" />
          </asp:RadioButtonList>
          <asp:RequiredFieldValidator ID="banquetPromotionValidator" runat="server" ControlToValidate="banquetPromotion" ErrorMessage="Please select whether or not you would like to receieve information about our promotions"></asp:RequiredFieldValidator>
            
          <br />
             <asp:Button runat="server" ID="myButton" OnClick="MakeReservation" Text="Submit"/>
          <br />
           
          <br />
           <asp:ValidationSummary 
            ID="validationSum"
            DisplayMode= "BulletList"
            runat="server"
            HeaderText="The following errors have occurred:"
            ShowMessageBox="false"
            ShowSummary="true"
            />

           <br />
           <div runat="server" id="reservationInfo">
              
            
           </div>
           <br />
           <br />

       </div>
    </form>
</body>
</html>

   