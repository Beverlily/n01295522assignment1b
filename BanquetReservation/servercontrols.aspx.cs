﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanquetBooking
{
    public partial class servercontrol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void DateValidator(object source, ServerValidateEventArgs args)
        {
            //reservation can't be made day of or made for a date in the past, checks to see if the reservation date is valid and if the reservation date is a date in the future
            args.IsValid = DateTime.TryParse(args.Value, out DateTime result) && result > DateTime.Today;
        }

        protected void MakeReservation(object sender, EventArgs e)
        {
            //if the form/page is not valid 
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                //client
                string cName = clientName.Text.ToString();
                string cEmail = clientEmail.Text.ToString();
                string cPhone = clientPhone.Text.ToString();

                //Client reservationClient = new Client(cName, cEmail, cPhone);

                //setting Client variables using property accessor/setter method
                Client reservationClient = new Client();
                reservationClient.ClientName = cName;
                reservationClient.ClientEmail = cEmail;
                reservationClient.ClientPhone = cPhone;

                //reservation
                string rDate = reservationDate.Text.ToString();
                string rSession = sessionList.Text.ToString();
                int rNumGuest = int.Parse(reservationGuestNumber.Text);
                string banquetSelected = banquetLocationList.Text.ToString();
                string promotionSelected = banquetPromotion.Text.ToString();
                bool rPromotion;

                if (promotionSelected == "Yes")
                {
                    rPromotion = true;
                }
                else
                {
                    rPromotion = false;
                }

                //banquets
                Banquet reservationBanquet;

                //creates a banquet object based on the selected banquet location 
                if (banquetSelected == "Richmond Hill")
                {
                    reservationBanquet = new Banquet("Beverly's Richmond Hill Banquet Hall", "beverlysbanquet@richmondhill.com", "111 111 1111", "123 Richmond Road");
                }
                else if (banquetSelected == "Markham")
                {
                    reservationBanquet = new Banquet("Beverly's Markham Banquet Hall", "beverlysbanquet@markham.com", "111 111 2222", "123 Markham Road");

                }
                else //Toronto
                {
                    reservationBanquet = new Banquet("Beverly's Toronto Banquet Hall", "beverlysbanquet@toronto.com", "111 111 3333", "123 Toronto Road");
                }

                List<string> confirmationOptions = new List<string>();
                List<string> banquetFeatures = new List<string>();

                //Got the code to loop through the checkboxes from your example
                //loop for selected confirmation options
                foreach (Control control in confirmationOptionsList.Controls) //.Controls = list of controls in the confirmationOptionsList element
                {
                    if (control.GetType() == typeof(CheckBox))
                    {
                        CheckBox confirmationOptionList = (CheckBox)control;
                        if (confirmationOptionList.Checked)
                        {
                            confirmationOptions.Add(confirmationOptionList.Text);
                        }

                    }
                }

                //loop for selected features of the reservation
                foreach (Control control in featuresList.Controls) //.Controls = list of controls in the feauturesList element
                {
                    if (control.GetType() == typeof(CheckBox))
                    {
                        CheckBox featuresList = (CheckBox)control;
                        if (featuresList.Checked)
                        {
                            banquetFeatures.Add(featuresList.Text);
                        }

                    }
                }

                /*
                //printing for debugging purposes

                foreach(string options in confirmationOptions)
                {
                    System.Diagnostics.Debug.WriteLine(options);
                }

                foreach (string options in banquetFeatures)
                {
                    System.Diagnostics.Debug.WriteLine(options);
                }
                */

                //Makes the reservation object and sets the variables by using a constructor 
                Reservation guestReservation = new Reservation(rDate, rSession, rNumGuest, confirmationOptions, reservationClient, reservationBanquet, banquetFeatures, rPromotion);

                reservationInfo.InnerHtml = guestReservation.PrintReservation();

            }
        }
    }

}


