﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanquetBooking
{

    public class Client
    {
        private string clientName;
        private string clientEmail;
        private string clientPhone;

        public Client()
        {

        }

        /*
        public Client(string name, string email, string phone)
        {
            clientName = name;
            clientEmail = email;
            clientPhone = phone;
        }
        */

        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }

    }

}