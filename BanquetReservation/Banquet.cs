﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanquetBooking
{
    public class Banquet
    {
        private string banquetLocation;
        private string banquetEmail;
        private string banquetPhoneNumber;
        private string banquetAddress;

        public Banquet(string name, string email, string phone, string address)
        {
            banquetLocation = name;
            banquetEmail = email;
            banquetPhoneNumber = phone;
            banquetAddress = address;
        }

        public string BanquetLocation
        {
            get { return banquetLocation; }
            set { banquetLocation = value; }
        }

        public string BanquetEmail
        {
            get { return banquetEmail; }
            set { banquetEmail = value; }
        }

        public string BanquetPhoneNumber
        {
            get { return banquetPhoneNumber; }
            set { banquetPhoneNumber = value; }
        }

        public string BanquetAddress
        {
            get { return banquetAddress; }
            set { banquetAddress = value; }
        }
    }
}